// provider list
providers:("pepperstone";"gaincapital");
// dictionary of functions to load data
loadFuncs:`gaincapital`pepperstone!`gcLoad`psLoad;
// hdb directory to use
hdbdir:`:hdb;

loadedFiles:();
dateslist:();

// logging function
lg:{-1"[",(string .z.T),"] ",x};

// load gaincaptial data function
gcLoad:{[file;hdbdir]
 lg"Loading ",string file;
 // pass loadchunk params relevant to this provider
 .Q.fsn[loadchunk[file;;1b;`gaincapital;"  SPFF";hdbdir];file;350000]
 }

// load pepperstone data function
psLoad:{[file;hdbdir]
 lg"Loading ",string file;
 // pass loadchunk params relevant to this provider
 .Q.fsn[loadchunk[file;;0b;`pepperstone;"SPFF";hdbdir];file;350000]
 }

// generic function to load a chunk of data
loadchunk:{[file;data;headers;src;colstr;hdbdir]
 // load the data without headers if this isn't the first chunk for this file
 // or if csv file doesn't have headers (i.e. headers=0b)
 $[(file in loadedFiles) or not headers;
   // use colstr to define columns in this csv
   t:(colstr;",")0: data;
   // if there are headers, get rid of them & add file to loadedFiles
   [t:value flip (colstr;enlist ",")0: data;
    loadedFiles,:file]];

 // add column names & make table
 t:flip `sym`time`bid`ask!t;

 // get each date
 dates:exec distinct "d"$time from t;
 dateslist::distinct dateslist,dates;
 
 //add src, remove date from time column
 t:update date:"d"$time,src:src,time:"t"$time from t;

 // enumerate
 t:.Q.en[hdbdir;t];

 // save to hdb
 {[d;t;hdbdir]
  t:select from t where d=date;
  (` sv (.Q.par[hdbdir;d;`quote];`)) upsert t;
 }[;t;hdbdir] each dates;
 
 }

// function to sort HDB & apply attributes on disc
sortHDB:{[hdbdir;date]
 // get file path
 path:` sv (.Q.par[hdbdir;date;`quote];`);

 // sort by sym and src
 lg"Sorting HDB for ",string date;
 `sym`src xasc path;

 lg"Applying `p# attribute to sym column";
 // apply attribute
 @[path;`sym;`p#];
 }

// function to build file list & load it for a given provider
loadProvider:{[provider;hdbdir]
 lg"Building file list for ",provider;
 // get file list for this provider
 files:{[provider;x]hsym `$provider,"/",string x}[provider;] each key hsym `$provider;

 // load files using function for this provider
 loadFuncs[`$provider][;hdbdir] each files;

 // sort HDB
 sortHDB[hdbdir;] each dateslist;
 }

// load up all providers
loadProvider[;hdbdir] each providers;
