pairs:("USD_CAD";"EUR_JPY";"EUR_USD";"EUR_CHF";"USD_CHF";"EUR_GBP";"GBP_USD";"NZD_USD";"AUD_USD";"GBP_JPY";"USD_JPY";"CHF_JPY";"AUD_JPY";"AUD_NZD");
lg:{-1"[",(string .z.T),"] ",x};

{
 lg"Downloading zips from gaincapital for ",x,"...";
 // download zips for month
 system"wget -r -l1 -nH -np --quiet http://ratedata.gaincapital.com/2016/10%20October -P tmp/ -A \"*",x,"*.zip\" --cut-dirs=4";
 } each pairs;

lg"Extracting zips...";
// extract all zips
system"unzip 'tmp/.\\\\*.zip' -d tmp";

lg"Cleaning up zips...";
// delete all the zips
system"rm tmp/.\\\\*";
