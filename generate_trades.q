// load HDB for quote table
\l hdb

// logging function
lg:{-1"[",(string .z.T),"] ",x};

gentrades:{[d;hdbdir]
 lg"Generating trades for ",string d;

 // get temp quote table to work with
 q:`time xasc select from quote where date=d;
 // get indices for first of each ask value
 syms:exec value first each group ask by sym from q;

 path:` sv (.Q.par[hdbdir;d;`trade];`);

 // generate trades for each sym
 {[q;d;path;s]
   lg"Generating trades for ",string s;
   path upsert select date,time,sym,src,price:ask from (select from q where sym=s) where i in d[s]
  }[q;syms;path] each key syms;

 lg"Sorting";
 `sym`src xasc path;

 lg"Applying attribute";
 @[path;`sym;`p#];
 }

// run function for currently loaded hdb
// use current dir as hdbdir as \l has changed directory

gentrades[;`:.] each date;


