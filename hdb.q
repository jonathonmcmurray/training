// load hdb
\l hdb

// checking functions
// ------------------

// date
checkd:{[d]
 if[-14h <> type d;-2"Provide a valid date";:0b];
 if[not d in date;-2"Date not present in HDB";:0b];
 :1b
 }

// time
checkt:{[t]
 if[not (type t) in -16 -17 -18 -19h;-2"Provide valid time type";:0b];
 :1b
 }

// queries for use against hdb
// ---------------------------

//- get data for one day
getday:{[d;syms;srcs]
 // check argument
 if[not checkd[d];:()];

 r:select from quote where date=d;
 syms:$[` ~ syms;exec distinct sym from r;syms,()];
 srcs:$[` ~ srcs;exec distinct src from r;srcs,()];

 // select from HDB
 select from r where sym in syms,src in srcs
 }

//- find gaps in day
gaps:{[d;syms;srcs;t]
 // check arguments
 if[not checkt[t] and checkd[d];:()]; 

 // call getday[d] to get data for this day, update a flag if gap is larger than specified
 // select all rows where flag is set, count by sym & src to get gaps
 select gaps:count i by sym,src from 
  (update flag:1b by sym,src from getday[d;syms;srcs] where t < time - prev time) 
   where flag
 }

//- find the number of gaps for all syms & srcs every day
allgaps:{[syms;srcs;t]
 // check argument
 if[not checkt[t];:()];

 // make dummy table for over
 tab:([sym:`$();src:`$()] gaps:());
 // run over with dummy table, and delete dummy column after
 // over will run gaps function for each day and join-each to maintained state
 delete gaps from {[x;syms;srcs;y;z] y,'(`sym`src,`$string z) xcol gaps[z;syms;srcs;x]}[t;syms;srcs]/[tab;date]
 }

// find gaps & sizes for a given day
intervals:{[d;syms;srcs]
 // check argument
 if[not checkd[d];:()];
 // add interval between each quote to quote data
 update interval:0^time-prev time by sym,src from getday[d;syms;srcs]
 }

// get the sizes of gaps > t for a given day
gapsizes:{[d;syms;srcs;t]
 // check arguments
 if[not checkt[t] and checkd[d];:()];
 
 select from intervals[d;syms;srcs] where interval>t
 }

bbo:{[d;syms;srcs;st;et]
 // check args
 if[not checkd[d] and checkt[st] and checkt[et];:()];

 // generate bid & ask book using scan with dummy table
 book:update bidbook:{[x;y]x[y[0]]:y[1];x}\[()!();flip (src;bid)],
             askbook:{[x;y]x[y[0]]:y[1];x}\[()!();flip (src;ask)]
               // get data for relevant day with getday[d], filter on time from args
	       by sym from `time xasc select from getday[d;syms;srcs] where time within (st;et);

 // append best bid & ask, along with source for each to book table for return      
 book,' flip `bbid`bask`bbsrc`basrc!
  ((exec max each bidbook from book);	 //bbid
   (exec min each askbook from book);	 //bask
   (exec {x?max x}'[bidbook] from book); //bbsrc
   (exec {x?min x}'[askbook] from book)) //basrc
 }
