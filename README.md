A repository of scripts used in training exercises.

## Scripts

[download.q](https://github.com/jonathonmcmurray/saxo-training/wiki/download.q): script to download one month's worth of data for common currency pairs from gaincapital  
[load.q](https://github.com/jonathonmcmurray/saxo-training/wiki/load.q): script to load CSV data from different providers (gaincapital & pepperstone) into an HDB  
[hdb.q](https://github.com/jonathonmcmurray/saxo-training/wiki/hdb.q): script to load HDB and provide useful queries to run against HDB  
[generate_trades.q](https://github.com/jonathonmcmurray/saxo-training/wiki/generate_trades.q): script to generate synthetic trades for HDB containing only quotes

More info on each script on the [wiki](https://github.com/jonathonmcmurray/saxo-training/wiki)
